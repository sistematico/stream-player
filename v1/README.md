# Stream Player

Um player de audio simples e responsivo para streams usando o ShoutCast/IceCast.

## Instalação

- `git clone https://gitlab.com/sistematico/stream-player /var/www/player`
- Edite a url da stream em `index.html`

## Demo

- [Stream Player](http://player.lucasbrum.net)

## Ajude

Doe qualquer valor através do <a href="https://pag.ae/bfxkQW"><img src="https://img.shields.io/badge/doe-pagseguro-green"></a> ou <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWHJL387XNW96&source=url"><img src="https://img.shields.io/badge/doe-paypal-blue"></a>

## ScreenShot

![Screenshot][screenshot]

[screenshot]: https://gitlab.com/sistematico/stream-player/raw/master/stream.png "ScreenShot"

