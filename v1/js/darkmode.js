var estilo = document.getElementById("estilo");
var icone = document.getElementById("darkIcon");
document.getElementById("checkbox").checked = false;

if (window.location.hash) {
    var hash = window.location.hash.substring(1);
    if (hash == 'dark') {
        estilo.setAttribute("href", 'css/dark.css');
        document.getElementById("checkbox").checked = true;
        icone.classList.remove("fa-sun");
        icone.classList.add("fa-moon");
    } else {
        estilo.setAttribute("href", 'css/light.css');
        icone.classList.remove("fa-moon");
        icone.classList.add("fa-sun");
    }
}

document.getElementById("checkbox").onclick = function () { 
    if (document.getElementById("checkbox").checked) {
        estilo.setAttribute("href", 'css/dark.css');
        icone.classList.remove("fa-sun");
        icone.classList.add("fa-moon");
        location.hash = "dark"; 
    } else {
        estilo.setAttribute("href", 'css/light.css');
        icone.classList.remove("fa-moon");
        icone.classList.add("fa-sun");
        location.hash = "light"; 
    }
};