const title = document.getElementById('titulo');

let consulta = (url, callback) => {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let data = JSON.parse(this.responseText);
            callback(data.icestats.source[0].title);
        }
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send(null); 
};

consulta(info, function(response) {
     title.innerHTML = response;
});

setInterval( function() { 
    consulta(info, function(response) {
        title.innerHTML = response;
    });
}, 5000);

title.onmouseover = function (event) {
    title.classList.remove("titulo");
};
 
title.onmouseleave = function (event) {
    title.classList.add("titulo");
};